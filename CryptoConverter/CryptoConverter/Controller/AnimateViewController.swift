//
//  AnimateViewController.swift
//  CryptoConverter
//
//  Created by nurlans on 5/13/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import UIKit
import QuartzCore
class AnimateViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageView.image = UIImage(imageLiteralResourceName: "LaunchScreenImage")
            
            UIView.transition(with: self.imageView,
                              duration: 5.0,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.imageView.alpha = 0 }, completion: nil )
        }
       
    }
    
