//
//  ConvertViewController.swift
//  CryptoConverter
//
//  Created by nurlans on 4/26/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import UIKit
import SDWebImageSVGCoder

class ConvertViewController: UIViewController {
    
    var quoteBase: Quote?
    var quoteConvert: Quote?
    var quoteBaseClick = true
    
    @IBOutlet weak var QuoteBase: UIButton?
    @IBOutlet weak var QuoteConvert: UIButton?
    @IBOutlet weak var Sum: UITextField?
    @IBOutlet weak var ResultLabel: UILabel?
    
    @IBAction func ActionSumChanging(_ sender: Any) {
        convert()
    }
    @IBAction func QuoteBaseClicked(_ sender: Any) {
        quoteBaseClick = true
    }
    @IBAction func QuoteConvertClicked(_ sender: Any) {
        quoteBaseClick = false
    }
    
    func convert() {
        if (quoteBase != nil && quoteConvert != nil && Sum != nil ) {
           let converter = Converter(quoteBase:quoteBase!)
           let sumToConvert = Double(Sum?.text ?? "0") ?? 0
            ResultLabel?.text = converter.convert(quoteConvert: quoteConvert!, sumToConvert: sumToConvert)
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        NotificationCenter.default.addObserver(self,                                                                  selector:#selector(getQuote),
                                               name: NOTIFICATION_QUOTE_SELECT,
                                               object: nil)
    }
    
    @objc func getQuote(notification: Notification) {
        let quote = (notification.object as? Quote)!
        if quoteBaseClick  {
            quoteBase = quote
            QuoteBase?.setBackgroundImage(quote.getImage(), for: .normal)
            QuoteBase?.setTitle("",for: .normal)
        } else {
            quoteConvert = quote
            QuoteConvert?.setBackgroundImage(quote.getImage(), for: .normal)
            QuoteConvert?.setTitle("",for: .normal)
        }
        convert()
    }
      
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? QuoteTableViewController {
            destination.isMainTable = false
            destination.refreshQuotes()
        }        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
