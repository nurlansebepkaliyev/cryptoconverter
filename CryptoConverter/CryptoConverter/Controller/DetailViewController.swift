//
//  DetailViewController.swift
//  CryptoConverter
//
//  Created by nurlans on 4/19/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet var logoImage: UIImageView?
    @IBOutlet var idLabel: UILabel?
    @IBOutlet var nameLabel: UILabel?
    @IBOutlet var priceLabel: UILabel?
    @IBOutlet var priceDateLabel: UILabel?
    @IBOutlet var priceTimeStampLabel: UILabel?
    @IBOutlet var marketCapLabel: UILabel?
    @IBOutlet var circulatingSupplyLabel: UILabel?
    @IBOutlet var maxSupplyLabel: UILabel?
    @IBOutlet var highLabel: UILabel?
    @IBOutlet var highTimestampLabel: UILabel?

    var quote: Quote?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logoImage?.image = quote?.getImage()
        idLabel?.text = quote?.id
        nameLabel?.text = quote?.name
        priceLabel?.text = quote?.price
        priceDateLabel?.text = quote?.priceDate
        priceTimeStampLabel?.text = quote?.priceTimestamp
        marketCapLabel?.text = quote?.marketCap
        circulatingSupplyLabel?.text = quote?.circulatingSupply
        maxSupplyLabel?.text = quote?.maxSupply
        highLabel?.text = quote?.high
        highTimestampLabel?.text = quote?.highTimestamp
    }

}
