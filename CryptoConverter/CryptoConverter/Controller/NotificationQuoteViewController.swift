//
//  NotificatioViewController.swift
//  CryptoConverter
//
//  Created by nurlans on 4/25/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import UIKit

class NotificationQuoteViewController: UIViewController {

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        NotificationCenter.default.addObserver(self,
                                               selector:#selector(receiveNotification),
                                               name: NOTIFICATION_SEND_NAME,
                                               object: nil)
    }
    
    @objc func receiveNotification(notification: Notification) {
        if let dataArray = notification.object as? [Quote] {
            self.view.backgroundColor = .red
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
