//
//  ChooseQuoteViewController.swift
//  CryptoConverter
//
//  Created by nurlans on 4/26/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import UIKit
let NOTIFICATION_QUOTE_BASE = NSNotification.Name("notification_quote_base")
let NOTIFICATION_QUOTE_CONVERT = NSNotification.Name("notification_quote_convert")
let NOTIFICATION_SUM = NSNotification.Name("notification_sum")

class QuoteListViewController: UITableViewController {

     var provider: QuoteProvider?
     var dataArray: [Quote] = []
     
     override func viewDidLoad() {
        super.viewDidLoad()
        let provider = QuoteProvider()
        dataArray = provider.generateQuotes()
    }
     
     override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 80.0
     }
     override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return dataArray.count
     }

     
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseQuoteCellId", for: indexPath) as! ChooseQuoteCell
         
         let quote = dataArray[indexPath.row]
         
         cell.logoImage?.image = UIImage(named: quote.logoUrl)
         cell.nameLabel?.text = "\(quote.name)"
         
         return cell
     }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let quote = dataArray[indexPath.row]
            NotificationCenter.default.post(name: NOTIFICATION_QUOTE_BASE, object: quote)
        dismiss(animated: true, completion: nil)
        
    }
   
}
