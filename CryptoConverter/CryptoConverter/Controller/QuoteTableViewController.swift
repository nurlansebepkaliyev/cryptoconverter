//
//  QuoteTableViewController.swift
//  CryptoConverter
//
//  Created by nurlans on 4/18/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import UIKit
import SDWebImageSVGCoder
import AVFoundation

class QuoteTableViewController: UITableViewController, QuoteProviderDelegate {
   
    var isMainTable = true
    var cach = QuoteCached()
    var provider: QuoteProvider = QuoteProvider()
    var dataArray: [Quote] = []
    var dataArrayLost: [Quote] = []
    var btnSound: AVAudioPlayer?
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataArray = cach.loadData()
        tableView.reloadData()
        
        self.imageView.image = UIImage(imageLiteralResourceName: "LaunchScreenImage")
            
            UIView.transition(with: self.imageView,
                              duration: 4.0,
                              options: [.repeat, .transitionCrossDissolve],
                              animations: {
                                self.imageView.alpha = 0 }, completion: nil )
        
        if !UserDefaults.standard.bool(forKey: "isFirstEnter") {
            UserDefaults.standard.set(true,forKey: "isFirstEnter")
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "WelcomePage") as! UIViewController
                self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    func getQuotes(quotes: [Quote]) {
        cach.quotes = quotes
        cach.setData()
        dataArray = quotes.isEmpty ? cach.loadData() : quotes
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    @IBAction func refreshQuotes() {
        playSound()
        dataArrayLost = dataArray
        provider.delegate = self
        provider.requestQuotes()
    }
    
    func playSound(){
        let path = Bundle.main.path(forResource: "btn_sound.mp3", ofType:nil)!
        let url = URL(fileURLWithPath: path)

        do {
            btnSound = try AVAudioPlayer(contentsOf: url)
            btnSound?.play()
        } catch {
            print("couldn't load file")
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuoteCellId", for: indexPath) as! QuoteCell
        
        let quote = dataArray[indexPath.row]
                
        cell.rankLabel?.text = quote.rank
        cell.logoImage?.sd_setImage(with: URL(string: quote.getRightUrlforImage()))
        cell.nameLabel?.text = quote.name
        cell.symbolLabel?.text = quote.symbol
        cell.priceLabel?.text = quote.price
        
        if !dataArrayLost.isEmpty {
            let price1 = Double(quote.price ?? "0") ?? 0
            let price2 = Double(dataArrayLost[indexPath.row].price ?? "0") ?? 0
            if price1 > price2 {
                cell.priceLabel?.textColor = UIColor.green
            } else if price1 < price2{
                cell.priceLabel?.textColor = UIColor.red
            } else {
                cell.priceLabel?.textColor = UIColor.black
            }
        }
        cell.priceDateLabel?.text = quote.priceDate
        cell.priceTimestampLabel?.text = quote.priceTimestamp
        cell.marketLabel?.text = quote.marketCap
        cell.circLabel?.text = quote.circulatingSupply
        cell.maxSupplyLabel?.text = quote.maxSupply

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let quote = dataArray[indexPath.row]
        if !isMainTable {
            NotificationCenter.default.post(name: NOTIFICATION_QUOTE_SELECT, object: quote)

        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if isMainTable {
            if let destination = segue.destination as? DetailViewController, let cell = sender as? QuoteCell, let indexPath = tableView.indexPath(for: cell) {
                        let quote = dataArray[indexPath.row]
                        destination.quote = quote
                    }
        } else {
             dismiss(animated: true, completion: nil)
        }
    }
}
