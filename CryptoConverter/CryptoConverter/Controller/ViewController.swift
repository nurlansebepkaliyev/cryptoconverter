//
//  ViewController.swift
//  CryptoConverter
//
//  Created by nurlans on 4/14/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func buttonStartClicked() {
        let array = getQuotes()
        let str = array.map { $0.description }.joined(separator: "\n")
        
        outputLabel1.text = str
        
        let convert = Converter(baseQuote: array[0])
        
        let result = array.map {convert.convert(sum: 1, convertQuote: $0) }.joined(separator: "\n")
        
        outputLabel2.text = result
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

