//
//  Convert.swift
//  CryptoConverter
//
//  Created by nurlans on 4/28/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import Foundation

class Converter {
    var quoteBase: Quote
    
    init(quoteBase: Quote) {
        self.quoteBase = quoteBase
    }
    
    func convert(quoteConvert:Quote, sumToConvert:Double) -> String {
        let price1 = Double(quoteConvert.price ?? "0") ?? 0
        let price2 = Double(quoteBase.price ?? "0") ?? 0
        return "\(sumToConvert * price1 / price2)"
    }
}
