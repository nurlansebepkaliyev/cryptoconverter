//
//  Ld.swift
//  CryptoConverter
//
//  Created by nurlans on 4/19/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import Foundation
struct Ld {
    var priceChange:Double
    var priceChangePct:Double
    var volume:Double
    var volumeChange:Double
    var volumeChangePct:Double
    var marketCapChange:Double
    var marketCapChangePct:Double
}
