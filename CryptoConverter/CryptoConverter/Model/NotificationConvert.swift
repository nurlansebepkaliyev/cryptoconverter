//
//  NotificationConvert.swift
//  CryptoConverter
//
//  Created by nurlans on 4/27/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import Foundation



class NotificationConvert {

    func addParameters(quoteBase:Quote, quoteConvert:Quote, sum:Double) {
        if let quoteBase = quoteBase as? Quote{
            NotificationCenter.default.post(name: NOTIFICATION_SEND_NAME, object: quoteBase)
        }
        if let quoteBase = quoteBase as? Quote{
            NotificationCenter.default.post(name: NOTIFICATION_SEND_NAME, object: quoteConvert)
        }
        if let quoteBase = quoteBase as? Quote{
            NotificationCenter.default.post(name: NOTIFICATION_SEND_NAME, object: sum)
        }
    }
        
}
