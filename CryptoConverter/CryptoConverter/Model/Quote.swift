//
//  QuoteForJson.swift
//  CryptoConverter
//
//  Created by nurlans on 4/29/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import Foundation
import SDWebImageSVGCoder

struct Quote: Decodable {
    var id: String
    var currency: String?
    var symbol: String?
    var name: String?
    var logoUrl: String?
    var rank: String?
    var price: String?
    var priceDate: String?
    var priceTimestamp: String?
    var marketCap: String?
    var circulatingSupply: String?
    var maxSupply: String?
    var high: String?
    var highTimestamp: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case currency
        case symbol
        case name
        case logoUrl = "logo_url"
        case rank
        case price
        case priceDate = "price_date"
        case priceTimestamp = "price_timestamp"
        case marketCap = "market_cap"
        case circulatingSupply = "circulating_supply"
        case maxSupply = "max_supply"
        case high
        case highTimestamp = "high_timestamp"
      }
    
    func getImage() -> UIImage? {
        let image = UIImageView()
        image.sd_setImage(with: URL(string: logoUrl!))
        return image.image
    }
    
    func getRightUrlforImage() -> String {
        let arrayUrl = [
            "DAI" : "https://cdn.publish0x.com/prod/fs/images/6c213ebaae9a6a1a9bfa608858e811b767435d3e90152e53bd39c422bc7b44a6.jpeg",
            "XVG" : "https://www.worldcryptoindex.com/wp-content/uploads/2018/01/verge-logo-300.jpg",
            "DRGN" : "https://f0.pngfuel.com/png/865/744/initial-coin-offering-dragon-coins-blockchain-cryptocurrency-coin-png-clip-art.png"
        ]
        return arrayUrl[id] ?? logoUrl!
    }
}
