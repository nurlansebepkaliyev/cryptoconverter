//
//  Quote.swift
//  CryptoConverter
//
//  Created by nurlans on 4/14/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import Foundation
struct Quote1 : CustomStringConvertible {
    var id:String
    var currency:String
    var symbol:String
    var name:String
    var logoUrl:String
    var rank:Int
    var price:Double
    var priceDate:String
    var priceTimestamp:String
    var marketCap:Double
    var circulatingSupply:Double
    var maxSupply:Double? // У USDT нет этого параметра
    var ld:Ld // вложенная структура
    var high:Double
    var highTimestamp:String

    var description: String {
        return "id: \(id)\ncurrency: \(currency)\nsymbol: \(symbol)\nname: \(name)\nrank: \(rank)\nprice: \(price)\nprice_date: \(priceDate):\nprice_timestamp: \(priceTimestamp)\nmarket_cap: \(marketCap)\ncirculating_supply: \(circulatingSupply)\nmax_supply: \(maxSupply ?? 0)\nld:\n[\nprice_change: \(ld.priceChange)\nprice_change_pct: \(ld.priceChangePct)\nvolume: \(ld.volume)\nvolume_change: \(ld.volumeChange)\nvolume_change_pct: \(ld.marketCapChangePct)\nmarket_cap_change: \(ld.marketCapChange)\nmarket_cap_change_pct: \(ld.marketCapChangePct)\n]\nhigh: \(high)\nhigh_timestamp: \(highTimestamp)"
    }
}


