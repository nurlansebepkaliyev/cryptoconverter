//
//  QuoteCached.swift
//  CryptoConverter
//
//  Created by nurlans on 5/5/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import Foundation
import RealmSwift

class QuoteCached {
    var quotes: [Quote] = []
    
    func setData() {
        let realm = try! Realm()
            do {
                try realm.write {
                    for i in 0 ..< quotes.count {
                        realm.add(getObjectFromStruct(quote: quotes[i]), update: .modified)
                    }
                }
            } catch {
                print("Cashing failure")
            }
    }
    
    func loadData() -> [Quote] {
        
        var quoteArray:[Quote] = []
        let realm = try! Realm()
        let realmArray = realm.objects(QuoteRealm.self)
        for i in 0 ..< realmArray.count {
            quoteArray.append(getStructFromObject(quoteRealm: realmArray[i]))
        }
        return quoteArray
    }
    
    func getObjectFromStruct(quote: Quote) -> QuoteRealm {
        let quotesRealm = QuoteRealm()
        
        quotesRealm.id = quote.id
        quotesRealm.currency = quote.currency ?? ""
        quotesRealm.symbol = quote.symbol ?? ""
        quotesRealm.name = quote.name ?? ""
        quotesRealm.logoUrl = quote.logoUrl ?? ""
        quotesRealm.rank = quote.rank ?? ""
        quotesRealm.price = quote.price ?? ""
        quotesRealm.priceDate = quote.priceDate ?? ""
        quotesRealm.priceTimestamp = quote.priceTimestamp ?? ""
        quotesRealm.marketCap = quote.marketCap ?? ""
        quotesRealm.circulatingSupply = quote.circulatingSupply ?? ""
        quotesRealm.maxSupply = quote.maxSupply ?? ""
        quotesRealm.high = quote.high ?? ""
        quotesRealm.highTimestamp = quote.highTimestamp ?? ""
        
        return quotesRealm
    }
    
    func getStructFromObject(quoteRealm: QuoteRealm) -> Quote {
        return Quote(
            id : quoteRealm.id,
            currency : quoteRealm.currency,
            symbol : quoteRealm.symbol,
            name : quoteRealm.name,
            logoUrl : quoteRealm.logoUrl,
            rank : quoteRealm.rank,
            price : quoteRealm.price,
            priceDate : quoteRealm.priceDate,
            priceTimestamp : quoteRealm.priceTimestamp,
            marketCap : quoteRealm.marketCap,
            circulatingSupply : quoteRealm.circulatingSupply,
            maxSupply : quoteRealm.maxSupply,
            high : quoteRealm.high,
            highTimestamp : quoteRealm.highTimestamp
        )
    }
}
