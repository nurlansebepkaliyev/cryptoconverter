//
//  Converter.swift
//  CryptoConverter
//
//  Created by nurlans on 4/14/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import Foundation

protocol QuoteProviderDelegate {
    func getQuotes(quotes: [Quote])
}
class QuoteProvider {
    let quoteAddress = "https://api.nomics.com/v1/currencies/ticker?key=dd0a24498f93b3bc2d1d1e032ca37f22&format=json&interval=5m&convert=USD"
    var delegate: QuoteProviderDelegate?
    
    func requestQuotes() {
        loadQuotes()
    }
    var dataArray:[Quote] = []
    func loadQuotes() {
        if let url = URL(string: quoteAddress){
            let quoteLoadTask =
            URLSession.shared.dataTask(with: url) {
                [weak self]
                (data, response, error)
                in
                guard let self = self else {
                        return
                }
                if let data = data {
                    do {
                        self.dataArray = try JSONDecoder().decode([Quote].self,
                                                             from: data)
                            self.delegate?.getQuotes(quotes: self.dataArray)
                        
                    } catch {
                        print("Decoding JSON failure")
                    }
                }
            }
            quoteLoadTask.resume()
        }
    }
}
