//
//  RealmClass.swift
//  CryptoConverter
//
//  Created by nurlans on 5/5/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import Foundation
import RealmSwift

class QuoteRealm: Object {
    @objc dynamic var id = UUID().uuidString
    @objc dynamic var currency = ""
    @objc dynamic var symbol = ""
    @objc dynamic var name = ""
    @objc dynamic var logoUrl = ""
    @objc dynamic var rank = ""
    @objc dynamic var price = ""
    @objc dynamic var priceDate = ""
    @objc dynamic var priceTimestamp = ""
    @objc dynamic var marketCap = ""
    @objc dynamic var circulatingSupply = ""
    @objc dynamic var maxSupply = ""
    @objc dynamic var high = ""
    @objc dynamic var highTimestamp = ""
   
    override static func primaryKey() -> String? {
        return "id"
    }
}
