//
//  ChooseQuoteCell.swift
//  CryptoConverter
//
//  Created by nurlans on 4/26/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import UIKit

class ChooseQuoteCell: UITableViewCell {

    @IBOutlet var logoImage: UIImageView?
    @IBOutlet var nameLabel: UILabel?
    
}
