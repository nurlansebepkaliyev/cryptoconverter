//
//  QuoteCellTableViewCell.swift
//  CryptoConverter
//
//  Created by nurlans on 4/19/20.
//  Copyright © 2020 nurlans. All rights reserved.
//

import UIKit

class QuoteCell: UITableViewCell {

    @IBOutlet var rankLabel: UILabel?
    @IBOutlet var logoImage: UIImageView?
    @IBOutlet var symbolLabel: UILabel?
    @IBOutlet var nameLabel: UILabel?
    @IBOutlet var priceLabel: UILabel?
    @IBOutlet var priceDateLabel: UILabel?
    @IBOutlet var priceTimestampLabel: UILabel?
    @IBOutlet var marketLabel: UILabel?
    @IBOutlet var circLabel: UILabel?
    @IBOutlet var maxSupplyLabel: UILabel?

}
